import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import {BarChartLibModule} from 'bar-chart-lib'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BarChartLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
