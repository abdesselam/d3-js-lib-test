import { Component } from '@angular/core';
import { StatsBarChart } from './data/data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'd3-js-li-test';
  data = StatsBarChart; 
}
